package com.example.appstudents.export_to_file;


import com.example.appstudents.entity.AttachmentContent;
import com.example.appstudents.payload.object_dto.StudentArrivalObjectDTO;
import com.example.appstudents.repository.AttachmentContentRepository;
import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Stream;


@Service
@RequiredArgsConstructor
public class PdfExportService {


    private final AttachmentContentRepository attachmentContentRepository;


    public void pdfExportMethod(HttpServletResponse response, StudentArrivalObjectDTO studentArrivalObjectDTO) throws IOException, DocumentException {
        response.setContentType("application/pdf");
        String headerKey = "Content-Disposition";
        String headerValue = "attachment; filename=studentInfo.pdf";
        response.setHeader(headerKey, headerValue);
        Document document = new Document();
        ServletOutputStream outputStream = response.getOutputStream();
        PdfWriter writer = PdfWriter.getInstance(document, outputStream);
        document.open();
        Font font = FontFactory.getFont(FontFactory.COURIER_BOLD, 22, BaseColor.BLUE);
        Paragraph paragraph = new Paragraph("Student Record", font);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);
        document.add(Chunk.NEWLINE);

        font = FontFactory.getFont(FontFactory.COURIER_BOLD, 16, BaseColor.BLACK);
        Chunk chunk = new Chunk("Name : " + studentArrivalObjectDTO.getfirst_name(), font);
        document.add(chunk);
        document.add(Chunk.NEWLINE);

        font = FontFactory.getFont(FontFactory.COURIER_BOLD, 16, BaseColor.BLACK);
        chunk = new Chunk("Surname : " + studentArrivalObjectDTO.getlast_name(), font);
        document.add(chunk);
        document.add(Chunk.NEWLINE);

        font = FontFactory.getFont(FontFactory.COURIER_BOLD, 16, BaseColor.DARK_GRAY);
        chunk = new Chunk("Birth Date : " + studentArrivalObjectDTO.getbirth_date().toString(),font);
        document.add(chunk);
        document.add(Chunk.NEWLINE);

        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(studentArrivalObjectDTO.getattachment_id());
        Image image = Image.getInstance(attachmentContent.getBytes());
        image.scaleAbsolute(90, 100);
        image.setAbsolutePosition(450, 640);
        document.add(image);


        document.add(Chunk.NEWLINE);
        font = FontFactory.getFont(FontFactory.COURIER_BOLD, 22, BaseColor.BLUE);
        paragraph = new Paragraph("Student Arrival", font);
        paragraph.setAlignment(Element.ALIGN_CENTER);
        document.add(paragraph);
        document.add(Chunk.NEWLINE);

        PdfPTable table = new PdfPTable(2);

        Stream.of("To Come Time", "Leaving Time").forEach(columnTitle -> {
            PdfPCell header = new PdfPCell();
            header.setBackgroundColor(BaseColor.GRAY);
            header.setBorder(4);
            header.setHorizontalAlignment(Element.ALIGN_CENTER);
            header.setPhrase(new Phrase(columnTitle));
            table.addCell(header);
        });

        PdfPCell cell1=new PdfPCell(new Phrase(studentArrivalObjectDTO.getto_come_time().toString()));
        cell1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell1);

        PdfPCell cell2=new PdfPCell(new Phrase(studentArrivalObjectDTO.getleaving_time().toString()));
        cell2.setHorizontalAlignment(Element.ALIGN_CENTER);
        table.addCell(cell2);

        table.addCell(studentArrivalObjectDTO.getleaving_time().toString());
        PdfContentByte directContent = writer.getDirectContent();
        table.setTotalWidth(500);
        table.writeSelectedRows(0, -1, 30, 550, directContent);

        document.close();


    }

}
