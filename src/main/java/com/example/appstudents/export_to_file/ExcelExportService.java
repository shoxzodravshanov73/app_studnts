package com.example.appstudents.export_to_file;

import com.example.appstudents.payload.object_dto.StudentArrivalObjectDTO;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Service;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Service
public class ExcelExportService {

    public void excelExportMethod(List<StudentArrivalObjectDTO> allByStudentAndArrival, HttpServletResponse response) throws IOException {
        XSSFWorkbook workbook = new XSSFWorkbook();
        XSSFSheet sheet = workbook.createSheet("Students");
        Row row = sheet.createRow(0);
        XSSFCellStyle cellStyle = workbook.createCellStyle();
        XSSFFont font = workbook.createFont();
        font.setBold(true);
        font.setFontHeight(16);
        cellStyle.setFont(font);

        Cell cell = row.createCell(0);
        cell.setCellValue("Id");
        sheet.autoSizeColumn(0);
        cell.setCellStyle(cellStyle);

        cell = row.createCell(1);
        cell.setCellValue("FirstName");
        sheet.autoSizeColumn(1);
        cell.setCellStyle(cellStyle);


        cell = row.createCell(2);
        cell.setCellValue("LastName");
        sheet.autoSizeColumn(2);
        cell.setCellStyle(cellStyle);

        cell = row.createCell(3);
        cell.setCellValue("BirthDate");
        sheet.autoSizeColumn(3);
        cell.setCellStyle(cellStyle);

        cell = row.createCell(4);
        cell.setCellValue("PhotoId");
        sheet.autoSizeColumn(4);
        cell.setCellStyle(cellStyle);

        cell = row.createCell(5);
        cell.setCellValue("ToComeTime");
        sheet.autoSizeColumn(5);
        cell.setCellStyle(cellStyle);

        cell = row.createCell(6);
        cell.setCellValue("LeavingTime");
        sheet.autoSizeColumn(6);
        cell.setCellStyle(cellStyle);

        int rowCount = 1;
        for (StudentArrivalObjectDTO students : allByStudentAndArrival) {
            XSSFRow row1 = sheet.createRow(rowCount++);


            XSSFCell cell1 = row1.createCell(0);
            cell1.setCellValue(students.getid());
            sheet.autoSizeColumn(0);

            cell1 = row1.createCell(1);
            cell1.setCellValue(students.getfirst_name());
            sheet.autoSizeColumn(1);

            cell1 = row1.createCell(2);
            cell1.setCellValue(students.getlast_name());
            sheet.autoSizeColumn(2);

            cell1 = row1.createCell(3);
            cell1.setCellValue(students.getbirth_date().toString());
            sheet.autoSizeColumn(3);

            cell1 = row1.createCell(4);
            cell1.setCellValue(students.getattachment_id());
            sheet.autoSizeColumn(4);

            cell1 = row1.createCell(5);
            cell1.setCellValue(students.getto_come_time().toString());
            sheet.autoSizeColumn(5);

            cell1 = row1.createCell(6);
            cell1.setCellValue(students.getleaving_time().toString());
            sheet.autoSizeColumn(6);
        }

        ServletOutputStream outputStream = response.getOutputStream();
        workbook.write(outputStream);
    }
}
