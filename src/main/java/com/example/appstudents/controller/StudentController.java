package com.example.appstudents.controller;

import com.example.appstudents.payload.ApiResult;
import com.example.appstudents.payload.CustomPage;
import com.example.appstudents.payload.req_dto.StudentReqDTO;
import com.example.appstudents.payload.res_dto.StudentResDTO;
import com.example.appstudents.service.StudentService;
import com.itextpdf.text.DocumentException;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;

@RestController
@RequestMapping("/api/student")
@RequiredArgsConstructor
@Tag(name = "Talaba operatsiyalai", description = "Students")
public class StudentController {

    private final StudentService studentService;


    /**
     * Bu Method barcha talabalarni sahifalab olib beradi
     * @param page - nechunchi sahifani olib  kelishi
     * @param size - nechta talaba olib kelishi
     * @return talabalarni sahifasini qaytaradi
     */
    @Operation(summary = "Talabalarning listini olish")
    @GetMapping("/getAll")
    public ApiResult<CustomPage<StudentResDTO>> getAll(@RequestParam int page,
                                                       @RequestParam int size) {
        return studentService.getAll(page, size);
    }

    /**
     * Bu method yangi talaba qo'shish uchun
     * @param studentReqDTO client tomondan kelayotgan malumotlarni olib malumotlar bazasiga yuborish uchun
     * @return qo'shilgan talabani malumotlarini qaytaradi
     */
    @Operation(summary = "Talaba qo'shish")
    @PostMapping("/add")
    public ApiResult<StudentResDTO> add(@Valid @RequestBody StudentReqDTO studentReqDTO) {
        return studentService.add(studentReqDTO);

    }

    /**
     * Bu method excel file dagi talabalarni malumotini uqib olib uni malumotlar bazasiga yuborish uchun
     * @param request kelayotgan malumotni ushlab olish uchun
     * @return muvaffaqiyatli yoki muvaffaqiyatsiz bulganini qaytaradi
     * @throws IOException excel file dan malumoti o'qishdagi xato
     */
    @Operation(summary = "Excel file dan malumotlarni o'qib DB ga saqlash")
    @PostMapping("/importExcel")
    public ApiResult<?> importExcelFile(MultipartHttpServletRequest request) throws IOException {
        return studentService.importExcelFile(request);
    }


    /**
     * Bu method Talabani id si orqali uni malumotlarini oladi
     * @param id - malumoti olinishi kerak bulgan talaba id si
     * @return talabani malumotlarini qaytaradi
     */
    @Operation(summary = "Bitta Talabani id si orqali")
    @GetMapping("/getById/{id}")
    public ApiResult<StudentResDTO> getById(@PathVariable Long id) {
        return studentService.getById(id);
    }


    /**
     * Bu method talaba id si orqali malumotlarni olib tahrirlash uchun ishlatilgan
     * @param studentReqDTO tahrirlangan malumotni olib malumotlar bazasiga yuboradi
     * @param id malumoti tahrirlanioshi kerak bulgan talaba id si
     * @return tahrirlangan talabaning malumotlarini qaytaradi
     */
    @Operation(summary = "Talaba ma'lumotlarini tahrirlash")
    @PutMapping("/edit/{id}")
    public ApiResult<StudentResDTO> edit(@Valid @RequestBody StudentReqDTO studentReqDTO, @PathVariable Long id) {
        return studentService.edit(studentReqDTO, id);
    }

    /**
     * Bu method talabaning barcha malumotlarini hamda kelib ketish vaqtlarini ham excel file ga export qilish uchun ishlatildi
     * @param response malumotlarni  berib yuboradi
     * @return muvaffaqiatli yoki muvaffaqiyatsiz ligini qaytaradi
     * @throws IOException excel file ga malumotni chop etish da buladigan xato
     */
    @GetMapping("/getAllExportExcel")
    public ApiResult<?> getAllStudentArrival(HttpServletResponse response) throws IOException {
        return studentService.getAllStudentArrival(response);
    }

    /**
     * Bu method talabaning malumotlarini rasmini va kelib ketish jadvalini pdf faylga yozish uichun ishlatildi
     * @param response pdf file ga malumotlarni yuborish uchun
     * @param id malumoti pdf file ga yuboril;ayotgan talabaning id si
     * @return muvaffaqiatli yoki muvaffaqiyatsiz ligini qaytaradi
     * @throws DocumentException Pdf file yaratishda uchrashi mumkin bulgan xatolik
     * @throws IOException     Pdf file ga malumotni chop etish da buladigan xato
     */
    @GetMapping("/getStudentIntoToPdf/{id}")
    public ApiResult<?> getStudentIntoToPdf(HttpServletResponse response, @PathVariable Long id) throws DocumentException, IOException {
        return studentService.getStudentIntoToPdf(response,id);
    }

    /**
     * Bu method talaba id si orqali shu talabani uchirish
     * @param id
     * @return
     */
    @Operation(summary = "Talabani id si orqali o'chirish")
    @DeleteMapping("/delete/{id}")
    public ApiResult<?> delete(@PathVariable Long id) {
        return studentService.delete(id);
    }


}
