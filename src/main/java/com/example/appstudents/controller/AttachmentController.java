package com.example.appstudents.controller;

import com.example.appstudents.entity.Attachment;
import com.example.appstudents.payload.ApiResult;
import com.example.appstudents.payload.CustomPage;
import com.example.appstudents.service.AttachmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/attachment")
@Tag(name = "Fayllar bilan ishlovchi operatsiyalar", description = "Fayllar")
public class AttachmentController {

    @Autowired
    AttachmentService attachmentService;

    @Operation(summary = "Fayl ni tizimga yuklash")
    @PostMapping("/upload")
    public ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException {
        return attachmentService.upload(request);
    }


    @Operation(summary = "Fayl ma'lumotlarining listini sahifalab olish")
    @GetMapping("/info")
    public ApiResult<CustomPage<Attachment>> getAll(@RequestParam int page,
                                                    @RequestParam int size){
        return attachmentService.getAll(page,size);

    }

    @Operation(summary = "Faylni yuklab olish")
    @GetMapping("/download/{id}")
    public ApiResult<?> getFile(@PathVariable Long id, HttpServletResponse response) throws IOException {
        return attachmentService.getFile(id,response);
    }
}
