package com.example.appstudents.controller;

import com.example.appstudents.payload.ApiResult;
import com.example.appstudents.payload.CustomPage;
import com.example.appstudents.payload.req_dto.ArrivalReqDTO;
import com.example.appstudents.payload.res_dto.ArrivalResDTO;
import com.example.appstudents.service.ArrivalService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.amqp.core.Exchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;


@RestController
@RequestMapping("/api/arrival")
@Tag(name = "Talabaning kelib ketish operatsiyalari",description = "Arrival")
public class ArrivalController implements Serializable {

    @Autowired
    ArrivalService arrivalService;

    @Operation(summary = "Barcha kelib ketish ni sahifalab olish")
    @GetMapping("/getAll")
    public ApiResult<CustomPage<ArrivalResDTO>> getAll(@RequestParam int page,
                                                       @RequestParam int size){
        return arrivalService.getAll(page,size);
    }

    @Operation(summary = "Yangi kelib ketilgan vaqtni qo'shish")
    @PostMapping("/add")
    public ApiResult<ArrivalResDTO> add(@Valid @RequestBody ArrivalReqDTO arrivalReqDTO, String sendMessage) {
        return arrivalService.add(arrivalReqDTO);
    }

    @Operation(summary = "Bitta kelib ketish vaqtini id si orqali olish")
    @GetMapping("/getById/{id}")
    public ApiResult<ArrivalResDTO> getById(@PathVariable Long id){
        return arrivalService.getById(id);
    }

    @Operation(summary = "Kelib ketish vaqtini tahrirlash")
    @PutMapping("/edit/{id}")
    public ApiResult<ArrivalResDTO> edit(@Valid @RequestBody ArrivalReqDTO arrivalReqDTO, @PathVariable Long id){
        return arrivalService.edit(arrivalReqDTO,id);
    }


    @Operation(summary = "Kelib Ketish vaqtini o'chirish")
    @DeleteMapping("/delete/{id}")
    public ApiResult<?> delete(@PathVariable Long id){
        return arrivalService.delete(id);
    }


}
