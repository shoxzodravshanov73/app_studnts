package com.example.appstudents.config;


import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {

    public static final String QUEUE="ARRIVAL";
    public static final String EXCHANGE="DIRECT_EXCHANGE";
    public static final String ROUTING_KEY="SEND.ARRIVAL";


    public static final String DLEQUEQE="ERROR_ARRIVAL";
    public static final String DLE="DEAD_LETTER_EXCHANGE";
    public static final String DLEROUTING_KEY="DEAD_LETTER_SEND.ARRIVAL";


    /**
     * Exchange  Queue larni RoutingKey orqali
     * @return
     */
    @Bean
    public DirectExchange directExchange(){
        return new DirectExchange(EXCHANGE);
    }



    @Bean
    public Queue queue(){
        return QueueBuilder.durable(QUEUE)
                .withArgument("x-dead-letter-exchange",DLE)
                .withArgument("x-dead-letter-routing-key",DLEROUTING_KEY)
                .build();
    }


    @Bean
    public Binding binding(){
        return BindingBuilder.bind(queue()).to(directExchange()).with(ROUTING_KEY);
    }




    @Bean
    public DirectExchange deadLetterExchange(){
        return new DirectExchange(DLE);
    }

    @Bean
    public Queue dleQueue(){
        return QueueBuilder.durable(DLEQUEQE)
                .build();
    }


    @Bean
    public Binding dleBinding(){
        return BindingBuilder.bind(dleQueue()).to(deadLetterExchange()).with(DLEROUTING_KEY);
    }
}
