package com.example.appstudents.mapper;

import com.example.appstudents.entity.Students;
import com.example.appstudents.payload.res_dto.StudentResDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface StudentsMapper {

    //source bu jadvaldagi nomi  , target  resDTO dagi nomi
    @Mapping(source = "students.attachment.id",target = "attachmentId")
    StudentResDTO studentToDTO(Students students);
}
