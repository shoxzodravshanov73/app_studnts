package com.example.appstudents.mapper;

import com.example.appstudents.entity.Arrival;
import com.example.appstudents.entity.Students;
import com.example.appstudents.payload.res_dto.ArrivalSendLogResDTO;
import com.example.appstudents.payload.res_dto.StudentResDTO;

public class CustomMapper {

    public static StudentResDTO studentToDTO(Students students){
        return new StudentResDTO(
                students.getId(),
                students.getFirstName(),
                students.getLastName(),
                students.getBirthDate(),
                students.getAttachment().getId()
        );
    }
    public static ArrivalSendLogResDTO arrivalSendToDTO(Arrival arrival){
        return new ArrivalSendLogResDTO(
                arrival.getStudents().getId(),
                arrival.getToComeTime(),
                arrival.getLeavingTime()
        );
    }

}
