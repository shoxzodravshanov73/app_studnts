package com.example.appstudents.mapper;

import com.example.appstudents.entity.Arrival;
import com.example.appstudents.payload.res_dto.ArrivalResDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface ArrivalMapper {

    @Mapping(source = "arrival.students.id",target = "studentId")
    ArrivalResDTO arrivalToDTO(Arrival arrival);
}
