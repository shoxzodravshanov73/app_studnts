package com.example.appstudents.component;

import com.example.appstudents.payload.ApiResult;
import com.example.appstudents.repository.ArrivalRepository;
import com.example.appstudents.service.ArrivalService;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;



@EnableScheduling
@Component
@RequiredArgsConstructor
public class ScheduleComponent {

    private final ArrivalRepository arrivalRepository;
    private final ArrivalService arrivalService;


    /**
     * Bu method har 2 minutda app_Logs api ga kelib ketgan talabalarni kelib ketish vaqtini yuboradi
     */
//    @Scheduled(fixedRate = 120000L)
//    private void sendArrivalToLog(){
//        ApiResult<?> apiResult = arrivalService.sendToLog();
//        if (apiResult.isSuccess()){
//            arrivalRepository.updateSendToLOg();
//        }
//
//
//    }
}
