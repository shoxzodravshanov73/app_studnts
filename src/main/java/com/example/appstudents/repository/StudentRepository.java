package com.example.appstudents.repository;

import com.example.appstudents.entity.Students;
import com.example.appstudents.payload.object_dto.StudentArrivalObjectDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StudentRepository extends JpaRepository<Students,Long> {


    @Query(value = "select s.id,first_name, last_name, birth_date, attachment_id, to_come_time, leaving_time from student s\n" +
            "join arrival a on s.id = a.students_id", nativeQuery = true)
    List<StudentArrivalObjectDTO> getAllByStudentAndArrival();


    @Query(value = "select s.id,first_name, last_name, birth_date, attachment_id, to_come_time, leaving_time from student s\n" +
            "join arrival a on s.id = a.students_id\n" +
            "where s.id =:id",nativeQuery = true)
    StudentArrivalObjectDTO findByIdStudent(@Param("id") Long id);
}
