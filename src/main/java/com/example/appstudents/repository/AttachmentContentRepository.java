package com.example.appstudents.repository;

import com.example.appstudents.entity.AttachmentContent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent,Long> {

    AttachmentContent findByAttachmentId(Long attachment_id);

}
