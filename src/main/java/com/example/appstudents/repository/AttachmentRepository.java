package com.example.appstudents.repository;

import com.example.appstudents.entity.Attachment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AttachmentRepository extends JpaRepository<Attachment,Long> {

}
