package com.example.appstudents.repository;

import com.example.appstudents.entity.Arrival;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ArrivalRepository extends JpaRepository<Arrival,Long> {

    @Query(value = "select leaving_time,to_come_time,students_id from arrival where send_to_logs=false",nativeQuery = true)
    Arrival findSendToLogFalse();

//



    List<Arrival> findAllBySendToLogsFalse();

    @Transactional
    @Modifying
    @Query(value = "update Arrival set send_to_logs=true ",nativeQuery = true)
    void updateSendToLOg();





}
