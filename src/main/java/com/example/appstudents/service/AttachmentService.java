package com.example.appstudents.service;

import com.example.appstudents.entity.Attachment;
import com.example.appstudents.entity.AttachmentContent;
import com.example.appstudents.exception.RestException;
import com.example.appstudents.payload.ApiResult;
import com.example.appstudents.payload.CustomPage;
import com.example.appstudents.repository.AttachmentContentRepository;
import com.example.appstudents.repository.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;

    @Autowired
    AttachmentContentRepository  attachmentContentRepository;


    public ApiResult<?> upload(MultipartHttpServletRequest request) throws IOException {

        Iterator<String> fileNames = request.getFileNames();
        MultipartFile file = request.getFile(fileNames.next());
        if (file!=null){
            String originalFilename = file.getOriginalFilename();
            Attachment attachment=new Attachment(
                    originalFilename,
                    file.getContentType(),
                    file.getSize());
            Attachment save = attachmentRepository.save(attachment);
            AttachmentContent attachmentContent=new AttachmentContent(file.getBytes(),save);
            attachmentContentRepository.save(attachmentContent);
            return ApiResult.successResponse(attachment.getId());
        }
        return ApiResult.errorResponse("Error");

    }

    public ApiResult<CustomPage<Attachment>> getAll(int page, int size) {
        PageRequest pageable = PageRequest.of(page, size);
        Page<Attachment> attachmentPage = attachmentRepository.findAll(pageable);
        CustomPage<Attachment> attachmentCustomPage=attachmentCustomPage(attachmentPage);
        return ApiResult.successResponse(attachmentCustomPage);

    }


    public ApiResult<?> getFile(Long id, HttpServletResponse response) throws IOException {

        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "This photo not found"));

        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(id);
        if (attachmentContent==null)
            throw new RestException(HttpStatus.NO_CONTENT,"this image has not content");
        response.setHeader("Content-Disposition","attachment; filename=\""+attachment.getName()+"\"");
        response.setContentType(attachment.getContentType());
        FileCopyUtils.copy(attachmentContent.getBytes(),response.getOutputStream());
        return ApiResult.successResponse("Successfully");
    }

    public CustomPage<Attachment> attachmentCustomPage(Page<Attachment> attachmentPage){
        return  new CustomPage<>(
                attachmentPage.getContent(),
                attachmentPage.getNumberOfElements(),
                attachmentPage.getNumber(),
                attachmentPage.getTotalElements(),
                attachmentPage.getTotalPages(),
                attachmentPage.getSize()
        );
    }
}
