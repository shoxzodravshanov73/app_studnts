package com.example.appstudents.service;

import com.example.appstudents.entity.Attachment;
import com.example.appstudents.entity.Students;
import com.example.appstudents.export_to_file.ExcelExportService;
import com.example.appstudents.exception.RestException;
import com.example.appstudents.export_to_file.PdfExportService;
import com.example.appstudents.mapper.CustomMapper;
import com.example.appstudents.mapper.StudentsMapper;
import com.example.appstudents.payload.ApiResult;
import com.example.appstudents.payload.CustomPage;
import com.example.appstudents.payload.object_dto.StudentArrivalObjectDTO;
import com.example.appstudents.payload.req_dto.StudentReqDTO;
import com.example.appstudents.payload.res_dto.StudentResDTO;
import com.example.appstudents.repository.AttachmentRepository;
import com.example.appstudents.repository.StudentRepository;
import com.itextpdf.text.DocumentException;
import lombok.RequiredArgsConstructor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class StudentService {


    private final StudentRepository studentRepository;
    private final AttachmentRepository attachmentRepository;
    private final StudentsMapper studentsMapper;
    private final ExcelExportService excelExportService;
    private final PdfExportService pdfExportService;


    /**
     * Bu Method barcha talabalarni sahifalab olib beradi
     *
     * @param page - nechunchi sahifani olib  kelishi
     * @param size - nechta talaba olib kelishi
     * @return talabalarni sahifasini qaytaradi
     */
    public ApiResult<CustomPage<StudentResDTO>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Students> studentsPage = studentRepository.findAll(pageable);
        CustomPage<StudentResDTO> studentResDTOCustomPage = studentsCustomPage(studentsPage);
        return ApiResult.successResponse(studentResDTOCustomPage);

    }

    /**
     * Bu method yangi talaba qo'shish uchun
     *
     * @param studentReqDTO client tomondan kelayotgan malumotlarni olib malumotlar bazasiga yuborish uchun
     * @return qo'shilgan talabani malumotlarini qaytaradi
     */
    public ApiResult<StudentResDTO> add(StudentReqDTO studentReqDTO) {

        Students students = new Students();
        students.setFirstName(studentReqDTO.getFirstName());
        students.setLastName(studentReqDTO.getLastName());
        students.setBirthDate(studentReqDTO.getBirthDate());
        Attachment attachment = attachmentRepository.findById(studentReqDTO.getAttachmentId()).orElseThrow(() -> new RestException(HttpStatus.CONFLICT, "Not found photo"));
        students.setAttachment(attachment);
        Students save = studentRepository.save(students);
        return ApiResult.successResponse(studentsMapper.studentToDTO(save));
    }

    /**
     * Bu method excel file dagi talabalarni malumotini uqib olib uni malumotlar bazasiga yuborish uchun
     * @param request kelayotgan malumotni ushlab olish uchun
     * @return muvaffaqiyatli yoki muvaffaqiyatsiz bulganini qaytaradi
     * @throws IOException excel file dan malumoti o'qishdagi xato
     */
    @Transactional(rollbackFor = RestException.class)
    public ApiResult<?> importExcelFile(MultipartHttpServletRequest request) throws IOException {
        Iterator<String> fileNames = request.getFileNames();
        MultipartFile files = request.getFile(fileNames.next());


        if (files != null) {
            XSSFWorkbook workbook = new XSSFWorkbook(files.getInputStream());
            XSSFSheet workSheet = workbook.getSheetAt(0);
            for (int i = 0; i < workSheet.getPhysicalNumberOfRows(); i++) {

//                    if (i==4){
//                        throw new RestException(HttpStatus.CONFLICT,"Error");
//                    }
                if (i > 0) {
                    XSSFRow row = workSheet.getRow(i);
                    StudentReqDTO studentReqDTO = new StudentReqDTO();
                    studentReqDTO.setFirstName(row.getCell(1).getStringCellValue());
                    studentReqDTO.setLastName(row.getCell(2).getStringCellValue());
                    studentReqDTO.setBirthDate(row.getCell(3).getDateCellValue());
                    studentReqDTO.setAttachmentId((long) row.getCell(4).getNumericCellValue());
                    add(studentReqDTO);
                }

            }
            return ApiResult.successResponse("Successfully import");
        }
        throw new RestException(HttpStatus.NOT_FOUND, "Null");
    }

    /**
     * Bu method Talabani id si orqali uni malumotlarini oladi
     * @param id - malumoti olinishi kerak bulgan talaba id si
     * @return talabani malumotlarini qaytaradi
     */
    public ApiResult<StudentResDTO> getById(Long id) {
        Students students = studentRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "This student not found"));
        return ApiResult.successResponse(studentsMapper.studentToDTO(students));

    }

    /**
     * Bu method talaba id si orqali malumotlarni olib tahrirlash uchun ishlatilgan
     * @param studentReqDTO tahrirlangan malumotni olib malumotlar bazasiga yuboradi
     * @param id malumoti tahrirlanioshi kerak bulgan talaba id si
     * @return tahrirlangan talabaning malumotlarini qaytaradi
     */
    public ApiResult<StudentResDTO> edit(StudentReqDTO studentReqDTO, Long id) {

        Students students = studentRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "This student not found"));
        students.setFirstName(studentReqDTO.getFirstName());
        students.setLastName(studentReqDTO.getLastName());
        Attachment attachment = attachmentRepository.findById(
                studentReqDTO.getAttachmentId()).orElseThrow(() ->
                new RestException(HttpStatus.NOT_FOUND, "This id image was not found"));

        students.setAttachment(attachment);
        students.setBirthDate(studentReqDTO.getBirthDate());
        Students save = studentRepository.save(students);
        return ApiResult.successResponse(studentsMapper.studentToDTO(save));
    }

    /**
     * Bu method talabaning barcha malumotlarini hamda kelib ketish vaqtlarini ham excel file ga export qilish uchun ishlatildi
     * @param response malumotlarni  berib yuboradi
     * @return muvaffaqiatli yoki muvaffaqiyatsiz ligini qaytaradi
     * @throws IOException excel file ga malumotni chop etish da buladigan xato
     */
    public ApiResult<?> getAllStudentArrival(HttpServletResponse response) throws IOException {
        List<StudentArrivalObjectDTO> allByStudentAndArrival = studentRepository.getAllByStudentAndArrival();
        if (allByStudentAndArrival.size() != 0) {
            response.setContentType("application/octet-stream");
            String headerKey = "Content-Disposition";
            String headerValue = "attachment; filename=students.xlsx";
            response.setHeader(headerKey, headerValue);
            excelExportService.excelExportMethod(allByStudentAndArrival, response);
            return ApiResult.successResponse("Successfully");
        }
        return ApiResult.errorResponse("Error");
    }

    /**
     * Bu method talabaning malumotlarini rasmini va kelib ketish jadvalini pdf faylga yozish uichun ishlatildi
     * @param response pdf file ga malumotlarni yuborish uchun
     * @param id malumoti pdf file ga yuboril;ayotgan talabaning id si
     * @return muvaffaqiatli yoki muvaffaqiyatsiz ligini qaytaradi
     * @throws DocumentException Pdf file yaratishda uchrashi mumkin bulgan xatolik
     * @throws IOException     Pdf file ga malumotni chop etish da buladigan xato
     */
    public ApiResult<?> getStudentIntoToPdf(HttpServletResponse response, Long id) throws DocumentException, IOException {
        StudentArrivalObjectDTO arrivalObjectDTO = studentRepository.findByIdStudent(id);
        if (arrivalObjectDTO != null) {
            pdfExportService.pdfExportMethod(response, arrivalObjectDTO);
            return ApiResult.successResponse("Successfully");
        }
        return ApiResult.errorResponse("Student Not Found");
    }

    public ApiResult<?> delete(Long id) {
        try {
            studentRepository.deleteById(id);
            return ApiResult.successResponse("Successfully deleted");
        } catch (Exception e) {
            e.printStackTrace();
            throw new RestException(HttpStatus.CONFLICT, "Error");
        }
    }

    /**
     * Bu method Student larni sahifalab olish uchun qo'lda yozildi
     * @param studentsPage
     * @return
     */
    public CustomPage<StudentResDTO> studentsCustomPage(Page<Students> studentsPage) {
        return new CustomPage<>(
                studentsPage.getContent().stream().map(CustomMapper::studentToDTO).collect(Collectors.toList()),
                studentsPage.getNumberOfElements(),
                studentsPage.getNumber(),
                studentsPage.getTotalElements(),
                studentsPage.getTotalPages(),
                studentsPage.getSize()
        );
    }


//    private String getCellValue(Row row, int cellNo){
//        DataFormatter formatter=new DataFormatter();
//        Cell cell = row.getCell(cellNo);
//        return formatter.formatCellValue(cell);
//    }

}
