package com.example.appstudents.service;

import com.example.appstudents.config.RabbitMQConfig;
import com.example.appstudents.entity.Arrival;
import com.example.appstudents.entity.Students;
import com.example.appstudents.exception.RestException;
import com.example.appstudents.mapper.ArrivalMapper;
import com.example.appstudents.mapper.CustomMapper;
import com.example.appstudents.payload.ApiResult;
import com.example.appstudents.payload.CustomPage;
import com.example.appstudents.payload.req_dto.ArrivalReqDTO;
import com.example.appstudents.payload.res_dto.ArrivalResDTO;
import com.example.appstudents.repository.ArrivalRepository;
import com.example.appstudents.repository.StudentRepository;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ArrivalService {

    private final ArrivalRepository arrivalRepository;
    private final StudentRepository studentRepository;
    private final ArrivalMapper arrivalMapper;
    private final RestTemplate restTemplate;
    private final RabbitTemplate rabbitTemplate;
    private final Gson gson;


    public ApiResult<CustomPage<ArrivalResDTO>> getAll(int page, int size) {
        Pageable pageable = PageRequest.of(page, size);
        Page<Arrival> arrivalPage = arrivalRepository.findAll(pageable);
        CustomPage<ArrivalResDTO> arrivalResDTOCustomPage = arrivalResDTOCustomPage(arrivalPage);
        return ApiResult.successResponse(arrivalResDTOCustomPage);
    }

    /**
     * Bu method talabaning kelib ketish vaqtini DB ga saqlaydi va bu malumotni app_logs ga junatadi
     * @param arrivalReqDTO
     * @return
     */
    public ApiResult<ArrivalResDTO> add(ArrivalReqDTO arrivalReqDTO) {
        Students student = studentRepository.findById(arrivalReqDTO.getStudentId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "This student not found"));
        Arrival arrival=new Arrival(
                student,
                arrivalReqDTO.getToComeTime(),
                arrivalReqDTO.getLeavingTime()
        );
        Arrival save = arrivalRepository.save(arrival);

        /**
         * Malumotlarni rabbitmq orqali yuborayotganimiz uchun string tipidagi malumot qabul qiladi biz yuborayotgan
         * objectni gson orqali stringa ugirib olib yuborayapmiz
         */
        String arrivalRes = gson.toJson(CustomMapper.arrivalSendToDTO(save));

        /**
         * rabbitTemplate app_logs ga malumot yuborish uchun xizmat qiladi
         */
        rabbitTemplate.convertAndSend(RabbitMQConfig.EXCHANGE,RabbitMQConfig.ROUTING_KEY,arrivalRes);

        return ApiResult.successResponse(arrivalMapper.arrivalToDTO(save));

    }


    public ApiResult<ArrivalResDTO> getById(Long id) {
        Arrival arrival = arrivalRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "This arrival not found"));
        return ApiResult.successResponse(arrivalMapper.arrivalToDTO(arrival));
    }

    public ApiResult<ArrivalResDTO> edit(ArrivalReqDTO arrivalReqDTO, Long id) {

        Arrival arrival = arrivalRepository.findById(id).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "This arrival not found"));
        Students student = studentRepository.findById(arrivalReqDTO.getStudentId()).orElseThrow(() -> new RestException(HttpStatus.NOT_FOUND, "This student not found"));
        arrival.setStudents(student);
        arrival.setToComeTime(arrivalReqDTO.getToComeTime());
        arrival.setLeavingTime(arrivalReqDTO.getLeavingTime());
        Arrival save = arrivalRepository.save(arrival);
        return ApiResult.successResponse(arrivalMapper.arrivalToDTO(save));
    }

    public ApiResult<?> delete(Long id) {
        try{
            arrivalRepository.deleteById(id);
            return ApiResult.successResponse("Successfully");
        }catch (Exception e){
            e.printStackTrace();
            throw new RestException(HttpStatus.CONFLICT,"Error");
        }
    }


//    public ApiResult<?> sendToLog(){
//        List<Arrival> arrivalList = arrivalRepository.findAllBySendToLogsFalse();
//        String url="http://localhost:8081/api/arrival/schedule/add";
//        if (arrivalList.size()!=0){
//            List<ArrivalResDTO> arrivalResDTOS = arrivalList.stream().map(arrivalMapper::arrivalToDTO).collect(Collectors.toList());
//            ArrivalResDTO arrivalResDTO1 = restTemplate.postForObject(url, arrivalResDTOS, ArrivalResDTO.class);
//            return ApiResult.successResponse(arrivalResDTO1);
//        }
//        return ApiResult.errorResponse("Error");
//    }


    public CustomPage<ArrivalResDTO> arrivalResDTOCustomPage(Page<Arrival> arrivalPage){
        return new CustomPage<>(
                arrivalPage.getContent().stream().map(arrivalMapper::arrivalToDTO).collect(Collectors.toList()),
                arrivalPage.getNumberOfElements(),
                arrivalPage.getNumber(),
                arrivalPage.getTotalElements(),
                arrivalPage.getTotalPages(),
                arrivalPage.getSize()
        );
    }

}
