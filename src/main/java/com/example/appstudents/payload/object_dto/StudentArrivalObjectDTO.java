package com.example.appstudents.payload.object_dto;

import java.util.Date;

public interface StudentArrivalObjectDTO {

    Long getid();
    String getfirst_name();
    String getlast_name();
    Date getbirth_date();
    Long getattachment_id();
    Date getto_come_time();
    Date getleaving_time();
}
