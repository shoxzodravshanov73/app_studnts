package com.example.appstudents.payload.req_dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArrivalReqDTO {

    @NotNull(message = "Talaba id si bo'sh bulmasin")
    private Long studentId;

    @NotNull(message = "Kelgan vaqti bo'lishi kerak")
    private Date toComeTime;

    @NotNull(message = "Kelgan vaqti bo'lishi kerak")
    private Date leavingTime;
}
