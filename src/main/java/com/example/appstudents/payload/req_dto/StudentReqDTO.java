package com.example.appstudents.payload.req_dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentReqDTO {

    @NotEmpty(message = "Ism bo'sh bulmasligi kerak")
    private String firstName;

    private String lastName;

    @NotNull(message = "Tug'ulgan sanasi bo'sh bulmasin")
    private Date birthDate;

    @NotNull(message = "Rasm bo'sh bulmasin")
    private Long attachmentId;
}
