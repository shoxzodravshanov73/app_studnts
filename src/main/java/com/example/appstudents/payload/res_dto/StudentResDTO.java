package com.example.appstudents.payload.res_dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentResDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private Date birthDate;
    private Long attachmentId;
}
