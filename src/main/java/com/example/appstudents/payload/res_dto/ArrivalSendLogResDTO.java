package com.example.appstudents.payload.res_dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ArrivalSendLogResDTO {

    private Long studentId;
    private Date toComeTime;
    private Date leavingTime;
}
