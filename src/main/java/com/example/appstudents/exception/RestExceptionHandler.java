package com.example.appstudents.exception;

import com.example.appstudents.payload.ApiResult;
import com.example.appstudents.payload.ErrorData;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.ArrayList;
import java.util.List;

@RestControllerAdvice
public class RestExceptionHandler {


    @ExceptionHandler(value = RestException.class)
    public ResponseEntity<?> exceptionHandling(RestException restException){
        restException.printStackTrace();
        return ResponseEntity.
                status(restException.getStatus()).
                body(ApiResult.errorResponse(restException.getMessage(),restException.getObject()));
    }

    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public ResponseEntity<?> exceptionHandling(MethodArgumentNotValidException e){
        List<ErrorData> errorData=new ArrayList<>();
        e.getBindingResult().getAllErrors().forEach(objectError -> errorData.add(
                new ErrorData(objectError.getDefaultMessage(),400)
        ));
        return ResponseEntity.status(400).body(ApiResult.errorResponse(errorData));
    }
}
